[Thorgate Management](http://thorgate.eu) general Python tools package *tgmtools*
=================================================================================

This package contains a set of general purpose functions and 
tools for use in other Python projects.
