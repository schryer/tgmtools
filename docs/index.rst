.. TGMtools documentation master file

.. _tgmtools-main:

########
TGMtools
########

*TGMtools* is Python package that provides general tools for use in other Python projects.

.. _tgmtools-import:

++++++++++++++++++++++++++++
Importing tgmtools functions
++++++++++++++++++++++++++++

The functions within some :obj:`tgmtools` sub packages are available within the top level module.

.. code-block:: python

    import tgmtools
    from tgmtools import core
    from tgmtools import obsfuscate
    assert tgmtools.pp == core.pp
    assert tgmtools.decode_path == obsfuscate.decode_path

For all examples provided we will assume the following has been imported:

.. code-block:: python

    import tgmtools as tgm

.. toctree::
   :maxdepth: 2

   examples/index.rst
   development/index.rst
   tgmtools_API.rst
   docExtraSidebar.rst

