=======================
Development of tgmtools
=======================

Because the :obj:`tgmtools` package provides tools that accomplish a wide
range of tasks, the main package has been structured into submodules and
subpackages.

++++++++++++++++++++++++++++++++++
Principles of package organization
++++++++++++++++++++++++++++++++++

Each submodule and subpackage contain tools that logically group together.
To make importing easier, the most commonly used objects are available at
the top module level (see :ref:`tgmtools-import`). General functions that
are commonly used and have no external dependencies are contained within
:mod:`~tgmtools.core`.

.. note::

    *The main organizing principle* is to group tools that perform related
    tasks within the same module, if this becomes unwieldy, create a subpackage.

++++++++++++++++++++++++++++++
Writing and running unit tests
++++++++++++++++++++++++++++++

Each subpackage contains a directory **tests** that contains test modules.
Typically, each submodule within the :obj:`tgmtools` package contains a sister
test module within its respective **tests** directory that starts with **test_**.
Each test module contains typically contains tests that related to one particular
submodule.

Start test functions with **test_**
===================================

The functions with each test module begin with **test_**.  This allows
`nose`_ to find and run them.

Run tests from the top level directory
======================================

To run all tests, execute the following from the top level :obj:`tgmtools` directory:

.. code-block:: bash

    nosetests -v

Please refer to the `nose`_ documentation for advanced usage.

++++++++++++++++++++++++++
Building the documentation
++++++++++++++++++++++++++

Documentation is built using `sphinx`_ locally, however, `ReadTheDocs`_
builds and hosts the documentation after every git push to `BitBucket`_.

.. _BitBucket: https://bitbucket.org/schryer/tgmtools

.. _ReadTheDocs:  http://tgmtools.readthedocs.org/en/latest/

.. _sphinx: http://sphinx-doc.org/

.. _nose: https://nose.readthedocs.org/en/latest/