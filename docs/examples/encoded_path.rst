.. _creating_encoded_path-example:

+++++++++++++++++++
Using encoded paths
+++++++++++++++++++

Encoded paths provide:

    - Safe encoding and decoding of UTF-8 filenames to ASCII.
    - A path that avoids subdirectory count problems.
    - Date and time information that does not rely on filesystem properties.
    - An obsfuscated structure that requires the original hash key to decode.
    - Directory names that rapidly fluctuate in time to simplify sharding operations.

Creating an encoded path to store a file under
==============================================

The function :func:`~tgmtools.create_encoded_path` is provided by the :mod:`~tgmtools.obsfuscate`
module within :obj:`tgmtools`, but can be imported at the top level.

>>> tgm.create_encoded_path == tgm.obsfuscate.create_encoded_path
True

The encoded path requires a filename, :obj:`~datetime.datetime` object, and a hash_key.

.. code-block:: python

    dt = datetime.datetime(2015, 4, 15, 1, 2, 3, 4)
    hk = '3f3vwIMFTkCnbWpxJgS7ZfqBNt8dWke2crGCvdwofMM='
    encoded_path = tgm.create_encoded_path('myfile.png', datetime_object=dt, hash_key=hk)

This creates a nested path structure that avoids subdirectory count problems
and provides obsfuscation.

>>> print(encoded_path)
9jIg/YRHrtP/Q336s6/r9LiMYtzKHvPhVDHjMUxeHGVFxLsln

Decoding an encoded path created using :func:`~tgmtools.create_encoded_path`
============================================================================

If you ever need to decode an encoded path, use the :func:`~tgmtools.decode_path` function
which returns the :obj:`~datetime.datetime` object and original filename.

>>> dt, filename = tgm.decode_path(encoded_path, hash_key=hk)
>>> dt, filename
(datetime.datetime(2015, 4, 15, 1, 2, 3, 4), 'myfile.png')
