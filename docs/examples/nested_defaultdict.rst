.. _nested_defaultdict-exmple:

++++++++++++++++++++++++++++++++
Using nested defaultdict objects
++++++++++++++++++++++++++++++++

The :obj:`defaultdict<collections.defaultdict>` container provided by the :mod:`collections` module
is a very useful structure. To extend its usefulness we provide convenience functions to nest them.

Creating a nested defaultdict
=============================

You can create nested containers by specifying the innermost type 
and the number of levels required (maximum five for now).

.. code-block:: python

	nested_dd = tgm.make_nested_defaultdict(set, N=3)
	counter = 0
	for key_A in ['B', 'A', 'X']:
	    for key_B in [99, 66]:
	        for key_C in ((3, 'A'), (2, 'B'), (1, 'C')):
		    for set_key in ('set_A', 'set_B'):
		        counter += 1
		        nested_dd[key_A][key_B][key_C][set_key].add(counter)
	
To better visualize these nested objects, :mod:`tgmtools` provides a
convenience function :func:`~tgmtools.core.pp` that prints the content of another convenience
function :func:`~tgmtools.core.pf` that wraps :func:`pprint.pformat`.

>>> tgm.pp(nested_dd)
{'A': {66: {(1, 'C'): {'set_A': {23},
                       'set_B': {24}},
	    (2, 'B'): {'set_A': {21},
		       'set_B': {22}},
	    (3, 'A'): {'set_A': {19},
                       'set_B': {20}}},
       99: {(1, 'C'): {'set_A': {17},
		       'set_B': {18}},
	    (2, 'B'): {'set_A': {15},
                       'set_B': {16}},
	    (3, 'A'): {'set_A': {13},
	               'set_B': {14}}}},
 'B': {66: {(1, 'C'): {'set_A': {11},
                       'set_B': {12}},
	    (2, 'B'): {'set_A': {9},
		       'set_B': {10}},
	    (3, 'A'): {'set_A': {7},
                       'set_B': {8}}},
       99: {(1, 'C'): {'set_A': {5},
                       'set_B': {6}},
            (2, 'B'): {'set_A': {3},
                       'set_B': {4}},
	    (3, 'A'): {'set_A': {1},
	               'set_B': {2}}}},
 'X': {66: {(1, 'C'): {'set_A': {35},
                       'set_B': {36}},
	    (2, 'B'): {'set_A': {33},
	               'set_B': {34}},
	    (3, 'A'): {'set_A': {31},
                       'set_B': {32}}},
       99: {(1, 'C'): {'set_A': {29},
                       'set_B': {30}},
            (2, 'B'): {'set_A': {27},
                       'set_B': {28}},
	    (3, 'A'): {'set_A': {25},
	               'set_B': {26}}}}}						  

Converting a nested defaultdict to pure Python
==============================================

If you need to convert a nested :obj:`~collections.defaultdict` to pure Python, :mod:`tgmtools` provides a
conversion function.

.. code-block:: python
        
	nested_dict = tgm.convert_nested_defaultdict_to_dict(nested_dd)
