.. _examples:

========
Examples
========

.. toctree::
   :maxdepth: 1
   
   nested_defaultdict.rst
   encoded_path.rst
