'''
Provides basic core functions to be used other Python projects.
'''

__all__ = ['TGMException',
           'convert_nested_defaultdict_to_dict', 'make_nested_defaultdict',
           'pf', 'pp',
           'pairwise',
           'memory_used']


class TGMException(Exception):
    '''
    Exception class used in the :mod:`tgmtools` project.
    '''
    pass

# ###########################  Functions based on the collections module  ###########################################


def convert_nested_defaultdict_to_dict(dd):
    '''
    Converts a nested :obj:`collections.defaultdict` object to a nested pure Python dictionary structure.

    Parameters
    ==========
    dd : :obj:`collections.defaultdict` object that is nested to any degree.

    Returns
    =======
    nested_dict : :obj:`dict` that is equivalent to the original object.
    '''
    import collections

    return {k: convert_nested_defaultdict_to_dict(v)
            for k, v in dd.items()} if isinstance(dd, collections.defaultdict) else dd


def make_nested_defaultdict(inner_type, N=1):
    '''
    Creates an  empty nested :obj:`collections.defaultdict` object.

    Parameters
    ==========
    inner_type : type of object at the innermost level of the nested :obj:`collections.defaultdict`.
    N : :obj:`int` of the number of nested levels (maximum five).

    Returns
    =======
    empty_nested_defaultdict : Nested :obj:`collections.defaultdict` objects that are empty.
    '''
    import collections

    dd = collections.defaultdict
    if N == 1:
        return dd(lambda: dd(inner_type))
    elif N == 2:
        return dd(lambda: dd(lambda: dd(inner_type)))
    elif N == 3:
        return dd(lambda: dd(lambda: dd(lambda: dd(inner_type))))
    elif N == 4:
        return dd(lambda: dd(lambda: dd(lambda: dd(lambda: dd(inner_type)))))
    elif N == 5:
        return dd(lambda: dd(lambda: dd(lambda: dd(lambda: dd(lambda: dd(inner_type))))))
    else:
        raise NotImplementedError('This level of nested defaultdict has not yet been implemented.', N)
#####################################################################################################################
#
#
# Functions used for text formatting


def pf(item):
    '''
    Convenience function that returns the output of :obj:`pprint.pformat`.
    '''
    import pprint
    return pprint.pformat(item)


def pp(item):
    '''
    Convenience function that prints out the output of :func:`.pf`
    '''
    print(pf(item))
######################################################################################################################
#
#
# Functions based on the itertools module


def pairwise(iterable):
    '''s -> (s0,s1), (s1,s2), (s2, s3), ...'''
    import itertools
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)
######################################################################################################################

# #############################  Functions used in debugging  ########################################################


def memory_used(unit='kB'):
    '''
    Output memory usage with specified units.

    Parameters
    ==========
    unit : :obj:`str` specifying unit (case insensitive). Must be a key in :attr:`~tgmtools.memory_used.all_units`.

    Attributes
    ==========
    all_units : :obj:`dict` of available units and conversion factors from kilobytes.

    Returns
    =======
    memory_used : :obj:`str` of memory usage with specified unit.
    '''
    import resource
    if unit.lower() not in memory_used.all_units.keys():
        raise Exception('Unit not recognised.', (unit, memory_used.all_units))

    usage_in_kb = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    return '{} {}'.format(usage_in_kb / memory_used.all_units[unit.lower()], unit)
memory_used.all_units = dict(kb=1, mb=1024, gb=1024*1024)
######################################################################################################################