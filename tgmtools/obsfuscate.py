'''
Provides basic file related functions to be used in other Python projects.
'''

__all__ = ['create_encoded_path', 'decode_path']

import os
import datetime

from hashids import Hashids

from . core import TGMException


def create_encoded_path(filename, hash_key=None, datetime_object=None):
    '''
    Creates an encoded path to store files in from a hash_key and a :mod:`datetime` object.

    Intended purpose is to prevent subdirectory count limits while providing obfuscation.

    Parameters
    ==========
    filename : :obj:`str` of filename to be stored
    hash_key : :obj:`str` key used during the hashing operation
    datetime_object : :mod:`datetime` object used during the hashing operation.

    Returns
    =======
    encoded_path : :obj:`str` of encoded path that can be decoded using :func:`.decode_path`.
    '''

    if not hash_key:
        raise TGMException('A hash_key must be supplied to encode a path.')

    if len(filename)*3 > 255:
        raise TGMException('The filename cannot exceed 85 characters.', len(filename))

    enc_engine = Hashids(hash_key)

    dt = datetime_object
    if not dt:
        dt = datetime.datetime.now()
    enc_mms = enc_engine.encode(dt.minute, dt.microsecond)
    enc_mds = enc_engine.encode(dt.month, dt.day, dt.second)
    enc_yh = enc_engine.encode(dt.year, dt.hour)

    enc_filename = enc_engine.encode(*[ord(c) for c in filename])

    return os.path.join(enc_mms, enc_mds, enc_yh, enc_filename)


def decode_path(encoded_path, hash_key=None):
    '''
    Decodes a path encoded by :func:`.create_encoded_path` using the same hash_key.

    Parameters
    ==========
    encoded_path : :obj:`str` of path encoded by encode_path using the same hash_key
    hash_key : :obj:`str` of key used by encode_path

    Returns
    =======
    datetime : :obj:`~datetime.datetime` object
    filename : :obj:`str` of original filename
    '''

    if not hash_key:
        raise TGMException('A hash_key must be supplied to encode a path.')

    enc_engine = Hashids(hash_key)

    enc_elements = encoded_path.split(os.sep)
    if len(enc_elements) != 4:
        msg = 'The encoded path must contain exactly four components.'
        raise TGMException(msg, len(enc_elements))

    enc_mms, enc_mds, enc_yh, enc_filename = enc_elements

    # Decode datetime object (move ms to end)
    minute, ms = list(enc_engine.decode(enc_mms))
    month, day, second = list(enc_engine.decode(enc_mds))
    year, hour = list(enc_engine.decode(enc_yh))
    datetime_list = [year, month, day, hour, minute, second, ms]
    datetime_object = datetime.datetime(*datetime_list)

    filename_numbers = enc_engine.decode(enc_filename)
    filename = ''.join([chr(n) for n in filename_numbers])

    return datetime_object, filename
