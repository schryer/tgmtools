'''
Used by nosetests
'''
import datetime

from tgmtools import TGMException, create_encoded_path, decode_path

testing_key = '3f3vwIMFTkCnbWpxJgS7ZfqBNt8dWke2crGCvdwofMM='

def test_encode_and_decode_path():

    # Times one millisecond or one second apart provide very
    # different directory and subdirectory names.
    test_date_1 = datetime.datetime(2015, 4, 15, 1, 2, 3, 4)
    test_date_2 = datetime.datetime(2015, 4, 15, 1, 2, 3, 5)
    test_date_3 = datetime.datetime(2015, 4, 15, 1, 2, 3, 6)
    test_date_4 = datetime.datetime(2015, 4, 15, 1, 2, 4, 6)

    for fn, test_date, expected in (('joe', test_date_1, '9jIg/YRHrtP/Q336s6/azncwjSAk'),
                                    ('blow', test_date_2, 'DwIJ/YRHrtP/Q336s6/LQaUXqCnDS8g'),
                                    ('blows', test_date_3, '1oIz/YRHrtP/Q336s6/A5gUrDiZnC9lURM'),
                                    ('hard', test_date_4, '1oIz/y7HWtB/Q336s6/zPxi4GIV2sAD'),
                                    ):
        print('Testing {} {}'.format(fn, expected))
        encoded_path = create_encoded_path(fn, datetime_object=test_date, hash_key=testing_key)
        assert encoded_path == expected, encoded_path

        date, filename = decode_path(encoded_path, hash_key=testing_key)
        assert date == test_date, date
        assert filename == fn, filename

    long_filename = 'long'*22
    try:
        create_encoded_path(long_filename, datetime_object=test_date, hash_key=testing_key)
    except TGMException as e:
        assert e.args[0] == 'The filename cannot exceed 85 characters.', e.args[0]
        assert e.args[1] == 88, e.args[1]