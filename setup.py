# Setup file to distribute tgmtools package using distutils.
from setuptools import find_packages
from distutils.core import setup

prj = 'tgmtools'

setup(name=prj,
      version='1.0',
      description='Python package containing general tools that can be used in other projects.',
      author='David Schryer',
      author_email='david@thorgate.eu',
      url='http://thorgate.eu/',
      install_requires=['hashids', 'nose'],
      packages=find_packages(exclude=['docs', 'tests*']),
      )
